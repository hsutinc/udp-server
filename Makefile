BINARY_NAME=udp_server
.PHONY: all

all: clean linux

linux:
	@echo "building linux client..."
	@GOOS=linux GOARCH=amd64 go build -o ./dist/$(BINARY_NAME) ./cmd/$(BINARY_NAME)

darwin:
	@echo "building darwin client..."
	@GOOS=darwin GOARCH=amd64 go build -o ./dist/$(BINARY_NAME) ./cmd/$(BINARY_NAME)

clean:
	@echo "cleaning previous build..."
	@rm -f ./dist/$(BINARY_NAME)