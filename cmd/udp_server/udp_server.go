package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

// UDPServer is responsible for sending data to udp client
func UDPServer(IPAddress string) error {
	raddr, err := net.ResolveUDPAddr("udp4", IPAddress)
	if err != nil {
		return errors.Wrapf(err, "Failed while resolving ip address: %s", IPAddress)
	}

	conn, err := net.DialUDP("udp4", nil, raddr)
	if err != nil {
		return errors.Wrapf(err, "Failed while dialing to udp client: %s", raddr)
	}

	fmt.Printf("The UDP client is %s\n", conn.RemoteAddr().String())
	defer conn.Close()

	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Print(">> ")
		text, _ := reader.ReadString('\n')

		var data []byte

		if packetSize, err := strconv.Atoi(strings.TrimSuffix(text, "\n")); err == nil {
			data = make([]byte, packetSize)
			fmt.Printf("Sending data: %v, with size: %d\n", data, len(data))
		} else {
			data = []byte(text)
			fmt.Printf("Sending data: %s, with size: %d\n", data, len(data))
		}

		_, err = conn.Write(data)

		if strings.ToUpper(strings.TrimSpace(text)) == "STOP" {
			fmt.Println("Exiting UDP client!")
			break
		}

		if err != nil {
			return errors.Wrap(err, "Failed while writing data")
		}
	}

	return nil
}
