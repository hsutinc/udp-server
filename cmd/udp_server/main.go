package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

// version is the package version
const version = "0.0.1"

// IPAddress is the remote address of the udp client to send message to
var IPAddress string

// var PacketSize int
// var PacketMSG string

func main() {
	var rootCmd = &cobra.Command{
		Use:     "udp_server",
		Short:   "udp server for block size simulation test",
		Long:    "udp server for block size simulation test by simulating the process of udp packet transmission via modulator and tv tuner to determine the proper block size",
		Version: version,
		Run: func(_ *cobra.Command, _ []string) {
			fmt.Println("===================================")
			fmt.Printf("IP address:\t%s\n", IPAddress)
			// fmt.Printf("Packet size:\t%d\n", PacketSize)
			// fmt.Printf("Packet message:\t%s\n", PacketMSG)
			fmt.Println("===================================")

			if err := UDPServer(IPAddress); err != nil {
				fmt.Printf("Failed executing UDPServer. %+v\n", err)
			}
		},
	}

	rootCmd.PersistentFlags().StringVarP(&IPAddress, "IPAddress", "a", "", "modulator's ip address")
	// rootCmd.PersistentFlags().IntVarP(&PacketSize, "packetSize", "p", 1024, "size of the packet to be sent")
	// rootCmd.PersistentFlags().StringVarP(&PacketMSG, "packetMSG", "m", "", "message of the packet to be sent")

	if c, err := rootCmd.ExecuteC(); err != nil {
		if c != nil {
			c.Printf("Run '%v --help' for usage.\n", c.CommandPath())
		}
	}
}
